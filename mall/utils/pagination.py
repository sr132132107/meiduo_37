from rest_framework.pagination import PageNumberPagination


class CustomPageNumberPagination(PageNumberPagination):

    #  设置每页返回的条目数
    page_size = 6

     # 开启前端分页功能
    page_size_query_param = 'page_size'