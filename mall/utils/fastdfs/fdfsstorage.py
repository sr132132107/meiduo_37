from django.core.files.storage import Storage
from fdfs_client.client import Fdfs_client
from django.utils.deconstruct import deconstructible
from mall import settings


# 自定义文件存储系统，因为配置文件的修改需要到配置文件中去修改，所以应该抽取函数

@deconstructible
class MyStorage(Storage):

    def __init__(self, config_path=None, ip=None):
        if not config_path:
            config_path = settings.FDFS_CLIENT_CONF
        self.config_path = config_path

        if not ip:
            ip = settings.FDFS_URL
        self.ip = ip





    #  Sorage必须要实现open和save两个方法
    def _open(self, name, mode='rb'):

        pass

    #  保存图片到远程地址并返回远程图片id
    def _save(self, name, content, max_length=None):


        #  创建client对象

        client = Fdfs_client(conf_path=self.config_path)


        # 获取图片内容，因为不能通过name来获取，所以需要content来读取

        data = content.read()   #  读取的结果为二进制

        #  上传图片


        result = client.upload_appender_by_buffer(data)


        #  判断上传结果

        if result.get('Status') == 'Upload successed.':

            #  上传成功，返回上传图片的file_id

            return result.get('Remote file_id')
        else:

            raise Exception('上传失败')


    #  判断上传的图片是否已经存在
    def exists(self, name):

        # 判断文件是否存在，FastDFS可以自行解决文件的重名问题
        # 所以此处返回False，告诉Django上传的都是新文件

        return False


    #  返回完整的url路径

    def url(self, name):

        return self.ip +name








