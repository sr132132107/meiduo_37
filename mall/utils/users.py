import re
from users.models import User

#  定义一个登录后返回响应携带数据的方法


from django.contrib.auth.backends import ModelBackend


def jwt_response_payload_handler(token,user=None,request=None):


    return {
        'token':token,
        'username':user.username,
        'userid':user.id
    }

#  抽取获取user的功能函数
def get_user_by_username(username):
    try:
        if re.match(r'1[3-9]\d{9}', username):
            user = User.objects.get(mobile=username)

        else:
            user = User.objects.get(username=username)
    except User.DoseNotExist:

        user =  None

    return user

#  重写后端认证的方法类


class UsernameMobileModelBackend(ModelBackend):


    def authenticate(self,request, username=None, password=None, **kwargs):

        # try:
        #     if re.match(r'1[3-9]\d{9}',username):
        #         user = User.objects.get(mobile = username)
        #
        #     else:
        #         user = User.objects.get(username = username)
        # except User.DoseNotExist:
        #
        #     return None
        #
        user = get_user_by_username(username)

        if user is not None and user.check_password(password):

            return user
        return None
