from rest_framework import serializers
from .models import SKU



#   商品sku序列化器

class SKUSerializer(serializers.ModelSerializer):


    class Meta:

        model = SKU

        fields=['id', 'name', 'price', 'default_image_url', 'comments']



#  建立sku商品索引类的序列化器

from .search_indexes import SKUIndex
from drf_haystack.serializers import HaystackSerializer

class SKUIndexSerializer(HaystackSerializer):
    """
    SKU索引结果数据序列化器
    """
    class Meta:
        index_classes = [SKUIndex]
        fields = ['text', 'id', 'name', 'price', 'default_image_url', 'comments']

