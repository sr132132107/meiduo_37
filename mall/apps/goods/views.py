from django.shortcuts import render

# Create your views here.



#  热销排行
"""
需求：
    当用户点击商品分类时进入到商品分类列表页，热销排行中会将该分类的商品按销量
    排序展示出来
步骤：
    1,接收数据   category_id

    2,查询数据库
    3,返回响应

    使用到序列化器器

视图和路由：
    三级视图   ListAPIView



    GET /goods/categories/(?P<category_id>\d+)/hotskus/





"""
from rest_framework.generics import ListAPIView
from .serializers import SKUSerializer
from .models import SKU
class HotSKUListView(ListAPIView):

    serializer_class = SKUSerializer

    pagination_class = None
    def get_queryset(self):
        category_id = self.kwargs['category_id']

        queryset = SKU.objects.filter(category_id=category_id,is_launched=True).order_by('-sales')[:2]

        return queryset



#  排序及分页处理

"""
需求：
    当用户点击商品分类时进入到商品分类列表页，排序分页中会将该分类的商品进行排序和分页处理
    展示出来
步骤：
    1,接收数据   category_id

    2,查询数据库
    3,返回响应

    使用到序列化器器

视图和路由：
    三级视图   ListAPIView



    GET /goods/categories/(?P<category_id>\d+)/skus/





"""
from rest_framework.filters import OrderingFilter



class SKUListView(ListAPIView):


    serializer_class = SKUSerializer

    # 指定排序及排序的字段
    filter_backends = [OrderingFilter]

    ordering_fields = ['price', 'create_time', 'sales']

    def get_queryset(self):

        category_id = self.kwargs['category_id']


        queryset = SKU.objects.filter(category_id=category_id,is_launched=True)




        return queryset



#  商品列表页，商品分类的静态化处理
"""
需求：
    商品分类不经常变化，使用静态化页面处理，在后台管理修改商品分类的时候才会发生变化

步骤：
    1,查询数据（即数据的生成）
    2,加载模板
    3,填充模板
    4,指定模板保存路径
    5,使用celery异步任务，在后台修改商品模型时加载页面

因只是生成静态的html页面数据，而且是在后台修改模型的时候触发加载页面，
所以不需要重新定义路由和视图

"""


#  建立SKU商品索引类
from .serializers import SKUIndexSerializer
from drf_haystack.viewsets import HaystackViewSet

class SKUSearchViewSet(HaystackViewSet):
    """
    SKU搜索
    """
    index_models = [SKU]

    serializer_class = SKUIndexSerializer






