from rest_framework import serializers
from .utils import check_access_token
from users.models import User
from django_redis import get_redis_connection
from .models import OAuthQQUser

class OauthQQUserSerializer(serializers.Serializer):

    access_token = serializers.CharField(label='操作凭证')
    mobile = serializers.RegexField(label='手机号', regex=r'^1[3-9]\d{9}$')
    password = serializers.CharField(label='密码', max_length=20, min_length=8)
    sms_code = serializers.CharField(label='短信验证码')


    #  验证数据
    def validate(self, attrs):

        access_token = attrs['access_token']

        openid = check_access_token(access_token)

        if openid is None:

            raise serializers.ValidationError('openid 过期')

        attrs['openid']=openid


        #  短信验证码的验证，验证该手机号是否仍在使用
        mobile = attrs['mobile']
        sms_code = attrs['sms_code']
        # 连接redis数据库获取短信验证码
        redis_conn = get_redis_connection('code')

        sms_code_redis = redis_conn.get('sms_%s'%mobile)

        if sms_code_redis is None:
            raise serializers.ValidationError('短信验证码已过期')
        if sms_code_redis.decode() != sms_code:
            raise serializers.ValidationError('短信验证码验证错误')


        #  password 验证
        password = attrs['password']

        #  获取User的实例化对象，调用该对象的check_password方法对password进行验证
        user = ''
        try:
            user = User.objects.get(mobile=mobile)
        except User.DoesNotExist:
            #  用户不存在，手机未注册过，需要注册

            pass
        else:
            #  用户存在判断用户的密码是否正确

            if user.check_password(password) == False:

                raise serializers.ValidationError('密码输入有误')

            attrs['user']= user

        return attrs


     #  数据在保存的时候调用save方法，触发了create方法，定义create方法

    def create(self, validated_data):

        user =validated_data.get('user')

        if user is None:
            user = User.objects.create(
                username=validated_data.get('mobile'),
                mobile=validated_data.get('mobile'),
                password=validated_data.get('password')
            )
            #  给用户密码加密
            user.set_password(validated_data.get('password'))
            #  修改完成之后要保存数据
            user.save()

        qquser = OAuthQQUser.objects.create(
            user=user,
            openid=validated_data.get('openid'))

        qquser.save()


        return qquser

