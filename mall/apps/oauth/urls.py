from django.conf.urls import url
from .views import OauthQQURLAPIView,OauthQQUserAPIView

urlpatterns = [
    url(r'^qq/statues/$',OauthQQURLAPIView.as_view()),
    url(r'^qq/users/$',OauthQQUserAPIView.as_view()),

]