from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.request import Request

# Create your views herpinjie e.


#  定义一个视图函数用来获取Authorization Code,
# 返回https://graph.qq.com/oauth2.0/authorize
# ？client_id= &response_type=code &redirect_uri= &state=

from QQLoginTool.QQtool import OAuthQQ
from mall import settings
class OauthQQURLAPIView(APIView):

    def get(self,request):

        #  拼接url字符串并返回
        # url = 'https://graph.qq.com/oauth2.0/authorize?client_id=101474184&response_type=code&redirect_uri=http://www.meiduo.site:8080/oauth_callback.html&state=test'

        #  实例化QQtool对象
        state = '/'
        oauthqq = OAuthQQ(client_id=settings.QQ_CLIENT_ID,
                          client_secret=settings.QQ_CLIENT_SECRET,
                          redirect_uri=settings.QQ_REDIRECT_URI,
                          state=state)
        auth_url = oauthqq.get_qq_url()
        return Response({'auth_url':auth_url})

#  通过code换token
#  定一个视图从前端获取code，并将code发送出去换取token
#  通过token换取openid
from rest_framework import status
from .models import OAuthQQUser

from .utils import generate_openid_token
from .serializers import OauthQQUserSerializer
class OauthQQUserAPIView(APIView):

    #  查询到用户已经绑定，直接登录
    def get(self,request):

        code  = request.query_params.get('code')
        if code is None:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        state = '/'
        oauthqq = OAuthQQ(client_id=settings.QQ_CLIENT_ID,
                          client_secret=settings.QQ_CLIENT_SECRET,
                          redirect_uri=settings.QQ_REDIRECT_URI,
                          state=state)
        try:
            access_token = oauthqq.get_access_token(code)

            openid = oauthqq.get_open_id(access_token=access_token)

        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)


        #  获取到openid，判断openid与用户是否绑定，绑定则直接返回用户信息登录
        #  未绑定，则需要与用户进行绑定
        #  根据openid查询数据库

        try:
            qquser = OAuthQQUser.objects.get(openid= openid)
        except OAuthQQUser.DoesNotExist:

            # 查询不到qquser则绑定用户,即将前端接收到的user与openid保存在数据库中
            #  因为openid是敏感数据，并且因该设置时效，所以在传递到前端的时候应该加密处理
            openid_token = generate_openid_token(openid)
            #返回响应
            return Response({'access_token':openid_token})


        #  查询到qquser生成token并登录
        else:
            from rest_framework_jwt.settings import api_settings
            jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
            jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
            payload = jwt_payload_handler(qquser.user)
            token = jwt_encode_handler(payload)

            return Response({
                'user_id':qquser.user.id,
                'username':qquser.user.username,
                'token':token

            })

    #  查询到用户未绑定，进行绑定
    def post(self,request):


        # 接收数据
        data = request.data
        # 校验数据
        serializer = OauthQQUserSerializer(data=data)
        serializer.is_valid()
        #  数据入库完成之后返回来的是一个模型对象
        qquser = serializer.save()
        #  返回响应
        from rest_framework_jwt.settings import api_settings

        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(qquser.user)
        token = jwt_encode_handler(payload)

        return Response({'token': token,
                         'username': qquser.user.username,
                         'user_id': qquser.user.id
                         })






