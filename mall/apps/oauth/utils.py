from itsdangerous import TimedJSONWebSignatureSerializer as serializer
from mall import settings
from itsdangerous import SignatureExpired,BadSignature
#  加密
def generate_openid_token(openid):
    #  加密序列化器的实例化
    s = serializer(secret_key=settings.SECRET_KEY, expires_in=3600)
    #  组织数据
    data = {
        'openid': openid
    }
    #  加密
    token = s.dumps(data)


    #  返回字符串
    return token.decode()

#  解密
def check_access_token(token):


    #  实例化序列化器

    s = serializer(secret_key=settings.SECRET_KEY,expires_in=3600)

    try:
        result = s.loads(token)

    except BadSignature:

        return None
    #  返回来的是一个字典数据
    return result.get('openid')


