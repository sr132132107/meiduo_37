from rest_framework import serializers

from .models import Area

#  定义地区序列化器


class AreaSerializer(serializers.ModelSerializer):

    class Meta:

        model = Area

        fields = ['id','name']


#  定义查询市区县的序列化器
class SubsAreaSerializer(serializers.ModelSerializer):
    subs = AreaSerializer(read_only=True, many=True)

    class Meta:

        model = Area

        fields = ['id','name','subs']









