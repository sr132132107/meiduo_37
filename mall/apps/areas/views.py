from django.shortcuts import render

# Create your views here.
from rest_framework.response import Response
from rest_framework.views import APIView

"""
需求：实现省市区的展示
    1,当用户点击添加地址按钮，省级选框里显示省份，市、县不显；当点击具体省份的时候，
    下属的市区显示
        省份视图（列表视图）
        下属视图（详情视图）
步骤：
    1,接收请求
    2,查询数据库
    3,返回响应   使用到序列化器

定义路由和视图：
    三级视图  ListAPIView  GET /areas/infos/

             RetireveAPIView    GET/areas/infos/(?P<id>\d+)/

编码：


"""
from rest_framework.generics import ListAPIView,RetrieveAPIView
from .serializers import AreaSerializer,SubsAreaSerializer
from .models import Area

# #  定义省份查询视图
# class ProvienceAPIView(ListAPIView):
#
#     serializer_class = AreaSerializer
#
#
#     def get_queryset(self):
#
#         queryset = Area.objects.filter(parent=None)
#
#         return queryset
#
#
#
# #  定义市区县查询视图
# class DistrictAPIView(RetrieveAPIView):
#
#     queryset = Area.objects.all()
#



from rest_framework.viewsets import ViewSet,ReadOnlyModelViewSet
from rest_framework_extensions.cache.mixins import CacheResponseMixin
#  使用视图集来获取省市区
class AreaModelViewSet(CacheResponseMixin,ReadOnlyModelViewSet):

    pagination_class = None

    def get_queryset(self):

        if self.action =='list':
            return Area.objects.filter(parent=None)

        else:
            return Area.objects.all()

    def get_serializer_class(self):

        if self.action =='list':

            return AreaSerializer

        else:

            return SubsAreaSerializer
























