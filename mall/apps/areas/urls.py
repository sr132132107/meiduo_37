from django.conf.urls import url
from areas import views
from .views import AreaModelViewSet
urlpatterns = [
    # url(r'^infos/$',views.ProvienceAPIView.as_view()),
    # url(r'^infos/(?P<pk>\d+)/$',views.DistrictAPIView.as_view()),
]

from rest_framework.routers import DefaultRouter

#  创建路由
router = DefaultRouter()

#  注册路由

router.register(r'infos',AreaModelViewSet,base_name='area')

#   添加路由到urlpatterns

urlpatterns+=router.urls
