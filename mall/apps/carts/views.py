from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from goods.models import SKU
from utils.exceptions import logger
from .serializers import CartSerializer,CartSKUSerializer
from django_redis import get_redis_connection
import pickle,base64
# Create your views here.



#  商品加入购物车
"""
需求：
    1,登录用户将商品信息（sku_id、count、selected选中状态、user）保存在redis中

    2,未登录用户将商品信息（sku_id、count、selected选中状态）保存在cookie中
步骤：
    # 1,接收数据（sku_id、count、selected选中状态、user）
    # 2,判断user的有无
    # 3,如果有，保存redis
    #     3.1, 验证数据  使用序列化器验证
    #     3.2,连接redis进行保存
    #         3.2.1, 组织redis数据   hset（user，sku，count）  sadd（select——user，select）
    #     3.3， 返回响应
    # 4,如果没有，保存cookie
    #     4.1, 接收cookie
    #     4.2,判断cookie的有无及时效
    #     4.3,如果有cookie，还原cookie数据 base64.base64decode() & pickle.loads()
    #     4.4,判断cookie中是否有购物车商品的信息
    #         4.4.1,如果没有，直接将购物车的信息加入到cookie中
    #         4.4.2,如果有，判断购物车里商品的信息sku_id,是否已存在，存在数量count增加，否在直接增加
    #     4.5,组织购物车cookie数据  dict类型
    #     4.6,cookie数据接行 pickle.dumps()二进制转换  byte类型
    #     4.7,base64.base64encode().decode()  数据加密编码和转换成字符串类型
    #     4.8,响应里response.set_cookie()
    5,返回响应

路由和视图的选取


    一级视图     APIView


     POST /cart/



编码：

"""

class CartAPIView(APIView):

    # 因为rest——framework添加了用户认证权限，所以需要重写认证方法
    #  这样登录用户和未登录用户均可添加购物车
    def perform_authentication(self, request):
        pass

    #  登录用户和未登录用户均可添加购物车，只是购物车的数据保存状态不一样
    def post(self,request):

        # 1,接收数据（sku_id、count、selected选中状态、user）
        data = request.data
        #  校验数据
        serializer = CartSerializer(data=data)

        serializer.is_valid(raise_exception=True)
        #  获取验证后的数据,因为后面要用

        sku_id = serializer.validated_data['sku_id']
        # sku_id = serializer.validated_data.get('sku_id')
        count = serializer.validated_data['count']
        selected = serializer.validated_data['selected']

        try:
            user = request.user
        except Exception as e:
            user = None


        # 2,判断user的有无
        if user is not None and user.is_authenticated:

            # 3,如果有，保存redis
            #  连接redis进行保存,即redis数据库的实例化
            redis_conn = get_redis_connection('cart')
            #  3.2.1, 组织redis数据进行保存   hset（user，sku，count）  sadd（select——user，select）
            # redis_conn.hset('cart_%s'%user.id,sku_id,count)
            #  应该考虑到当用户添加购物车时检查redis中是否已经存在该商品
            # redis_conn.hincrby('cart_%s'%user.id,sku_id,count)   #  如果之前没有数据则新增，有数据则累加
            #  使用set数据类型保存商品的选中状态
            # if selected:
            #     redis_conn.sadd('cart_selected_%s'%user.id,sku_id)

            #  使用管道技术来操作redis
            #  创建管道实例
            pl = redis_conn.pipeline()
            #  收集redis操作
            pl.hincrby('cart_%s'%user.id,sku_id,count)

            if selected:
                pl.sadd('cart_selected_%s'%user.id,sku_id)
            #  执行管道
            pl.execute()
            # 3.3， 返回响应
            return Response(serializer.data)

        else:
            # 4,如果没有，保存cookie
            #  4.1, 接收cookie
            cookie_cart = request.COOKIES.get('cart')   #  设置cookie时key应设为cart
            #  4.2,判断cookie的有无及时效
            if cookie_cart is not None:
            # 4.3,如果有cookie，还原cookie数据 base64.base64decode() & pickle.loads()
                cookie_cart_str = pickle.loads(base64.b64decode(cookie_cart))

            else:
                #  如果没有购物车的cookie，则直接初始化购物车cookie
                cookie_cart_str= {}
                # cookie_cart = {1: {'count': 2, 'selected': True}}

            #  判断还原后的cookie有没有商品信息

            if sku_id in cookie_cart_str:
                #  4.4.2,如果有，判断购物车里商品的信息sku_id,是否已存在，
                # 存在数量count增加，否在直接增加
                original_count = cookie_cart_str[sku_id]['count']
                count += original_count
            #   4.4.1,如果没有，直接将购物车的信息加入到cookie中
            cookie_cart_str[sku_id]={
                'count':count,
                'selected':selected
            }
            # 组织购物车cookie数据  dict类型,对数据进行编码加密
            cookie_save_str = base64.b64encode(pickle.dumps(cookie_cart_str)).decode()
            response = Response(serializer.data)
            # 响应里response.set_cookie()
            response.set_cookie('cart', cookie_save_str,3600)

            return response


    #  查询购物车状态
    """
    需求：
        登录用户和未登录用户，在查询我的购物车时，都应该展示商品的信息、购买状态和选中状态
    步骤：
        判断用户的登录状态
        接收参数   user和 携带cookie
        验证参数
        查询数据库
        返回响应

    GET /cart/

    """

    def get(self, request):

        try:
            user = request.user
        except Exception as e:
            user = None


        #  判断用户是否登录
        if user is not None and user.is_authenticated:
            #  登录用户  查询redis
            redis_conn = get_redis_connection('cart')

            #  获取保存在redis中的数据
            redis_ids_count = redis_conn.hgetall('cart_%s' % user.id)
            redis_selected_ids = redis_conn.smembers('cart_selected_%s' % user.id)

            # #  根据商品id来获取商品的详细信息
            # skus = []
            # #  对hash数据进行解包
            # for id,count in redis_ids_count.items():
            #     sku = SKU.objects.get(pk=id)
            #     #  因为商品模型里面没有count和selected属性，所以需要动态的添加该属性
            #     sku.count = count
            #     if id in redis_selected_ids:
            #         sku.selected = True
            #     else:
            #         sku.selected = False
            #     skus.append(sku)
            #
            # serializer = CartSKUSerializer(instance=skus,many=True)
            #
            # return Response(serializer.data)

            #  因为返回给登录用户和非登录用户的数据一样，所以将两种途径获取到的数据的数据格式进行统一
            #  将redis数据转换为cookie数据
            # hash
            # {sku_id:count,sku_id:count}
            # set
            # {sku_id,sku_id}
            #  cookie
            # {sku_id:{count:xxx,selected:xxx}}
            cookie_cart = {}
            for id,count in redis_ids_count.items():

                if id in redis_selected_ids:
                    selected = True

                else:
                    selected = False
                #  从redis中获得数据为byte类型，需要转换数据类型
                cookie_cart[int(id)]={
                    'count':int(count),
                    'selected':selected
                }


        else:
            #  未登录用户获取cookie信息
            cookie_str = request.COOKIES.get('cart')

            #  判断cookie是否存在或过期

            if cookie_str is not None:

                #  还原cookie数据
                cookie_cart = pickle.loads(base64.b64decode(cookie_str))

            else:
                #  cookie数据为空，则初始化cookie——cart
                cookie_cart = {}

                # {sku_id:{count:xxx,selected:xxx}}
                #  从cookie中获取商品信息
        ids = cookie_cart.keys()
        skus = SKU.objects.filter(pk__in=ids)
        #  动态的给商品对象增加属性
        for sku in skus:
            sku.count = cookie_cart[sku.id]['count']
            # sku.count = cookie_cart[sku.id]['count']
            sku.selected = cookie_cart[sku.id]['selected']

            #  通过序列化器返回商品的详细信息
        serializer = CartSKUSerializer(instance=skus, many=True)

        return Response(serializer.data)




    #  修改购物车

    """
    需求：
        接收前端发送的数据 user——id  sku_id  selected count ，进行处理保存


    """
    def put(self,request):


        data= request.data

        #  验证数据

        serializer = CartSerializer(data=data)

        serializer.is_valid(raise_exception=True)

        #  获取验证后的数据
        sku_id = serializer.validated_data['sku_id']
        count = serializer.validated_data['count']
        selected = serializer.validated_data['selected']
        #  判断用户是否登

        try:
            user = request.user
        except Exception as e:
            user = None

        if user is not None and user.is_authenticated:

            #  获取redis中的购物车信息
            redis_conn = get_redis_connection('cart')

            redis_ids_count = redis_conn.hgetall('cart_%s'%user.id)

            redis_ids_selected = redis_conn.smembers('cart_selected_%s'%user.id)

            #  解包判断获取的sku——id有没有在redis中保存过，保存过则直接count增加，没保存过则直接增加

            #  更新hash数据，直接覆盖，有则数据增加，没有则直接添加
            redis_conn.hset('cart_%s'%user.id,sku_id,count)
            #  更新set数据
            if selected:
                #  因为是集合数据，所以会自动去重，所以只管添加即可
                redis_conn.sadd('cart_selected_%s'%user.id,sku_id)
            else:
                #  将之前有的sku状态进行更新
                redis_conn.srem('cart_selected_%s'%user.id,sku_id)


            return Response(serializer.data)





        else:


            #  获取cookie中的数据
            cookie_str = request.COOKIES.get('cart')


            #  判断cookie数据有没有过期或是有没有商品信息

            if cookie_str is not None:

                cookie_cart = pickle.loads(base64.b64decode(cookie_str))

            else:
                cookie_cart = {}

            #  获取cookie中的商品id和count等状态

            if sku_id in cookie_cart:

               cookie_cart[sku_id]={
                   'count':count,
                   'selected':selected
               }

            #  组织cookie数据进行加密

            cookie_save_str = base64.b64encode(pickle.dumps(cookie_cart)).decode()

            response = Response(serializer.data)

            response.set_cookie('cart',cookie_save_str,3600)

            return response


    #  删除购物车数据
    def delete(self, request):

        # 1.接收商品id
        data = request.data
        # 2.验证商品id(验证省略)
        sku_id = data.get('sku_id')
        # 3.获取用户信息
        try:
            user = request.user
        except Exception:
            user = None
        # 4.根据用户进行判断
        if user is not None and user.is_authenticated:

            # 5.登陆用户操作redis
            #    5.1 连接redis
            redis_conn = get_redis_connection('cart')
            #    5.2 删除数据 hash,set
            # hash
            redis_conn.hdel('cart_%s' % user.id, sku_id)
            # set
            redis_conn.srem('cart_selected_%s' % user.id, sku_id)
            #    5.3 返回相应

            return Response(status=status.HTTP_204_NO_CONTENT)

        else:

            # 6.未登录用户操作cookie
            #    6.1 先获取cookie数据
            cookie_str = request.COOKIES.get('cart')
            #    6.2 判断cookie数据是否存在
            if cookie_str is not None:
                cookie_cart = pickle.loads(base64.b64decode(cookie_str))
            else:
                cookie_cart = {}
            # 6.3 删除
            if sku_id in cookie_cart:
                del cookie_cart[sku_id]
            # 6.4 将字典进行加密
            cookie_save_str = base64.b64encode(pickle.dumps(cookie_cart)).decode()
            #    6.5 设置cookie,返回相应
            response = Response(status=status.HTTP_204_NO_CONTENT)

            response.set_cookie('cart', cookie_save_str, 3600)

            return response







