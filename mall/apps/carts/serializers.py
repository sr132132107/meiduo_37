from rest_framework import serializers
from goods.models import SKU

#  定义购物车商品序列化器
class CartSerializer(serializers.Serializer):
    # sku_id
    # count
    # selected 选填
    sku_id = serializers.IntegerField(label='商品id',required=True,min_value=0)
    count = serializers.IntegerField(label='个数',required=True,min_value=1)
    selected = serializers.BooleanField(label='是否选中',required=False,default=True)



    #  验证数据  验证商品id存在和购买的数量不应大于库存量

    def validate(self, attrs):
        sku_id = attrs['sku_id']
        count = attrs['count']

        try:
            sku = SKU.objects.get(pk=sku_id)
        except SKU.DoesNotExist:
            raise serializers.ValidationError('商品不存在')

        stock_count = sku.stock

        if count > stock_count:

            raise serializers.ValidationError('商品库存不足')

        return attrs



#  定义购物车展示的序列化器和修改购物车使用

class CartSKUSerializer(serializers.ModelSerializer):
    count = serializers.IntegerField(label='数量')
    selected = serializers.BooleanField(label='是否勾选')

    class Meta:
        model = SKU
        fields = ('id', 'count', 'name', 'default_image_url', 'price', 'selected')