





"""
需求：
    登录的时候合并cookie和redis中的数据到redis中
步骤：
    获取cookie数据
      {sku_id:{count:xxx,selected:xxx},}

        {1: {count:10,selected:True}, 3:{count:30,selected:False}}
     2.获取redis (从redis服务器上将数据获取下来,不要通过redis代码来判断,
                因为判断会增加redids服务器的交互)
        hash:   {sku_id:count}
                {b'2':b'20',b'3':b'100'}
        set:    {sku_id}
                {2,3}

        我们把最终的数据再更新到 redis中
        最终的: {2:20,3:30,1:10}
        选中的: {1}
    合并数据
    合并前初始化记录 ( redis的数据 原则是不动的)
        cart = {2:20,3:100}
        cart_selected = {}


    合并
        1: {count:10,selected:True}
            cart = {2:20,3:100} + {1:10}
            cart_selected = {1}

        3:{count:30,selected:False}
            cart = {2:20,3:30} + {1:10}
            cart_selected = {1}

     最终的数据
        cart = {2:20,3:30,1:10}
        cart_selected = {1}


"""
import base64
import pickle
from django_redis import get_redis_connection


def merge_cookie_to_redis(request,user,response):


    cookie_str = request.COOKIES.get('cart')
    if cookie_str is not None:
        # 说明有数据
        cookie_cart = pickle.loads(base64.b64decode(cookie_str))

        #  获取redis数据

        redis_conn = get_redis_connection('cart')

        # hash
        redis_ids_count = redis_conn.hgetall('cart_%s'%user.id)

        #  redis数据进行遍历，遍历的时候转换数据类型
        redis_cart = {}
        for sku_id,count in redis_ids_count.items():
            redis_cart[int(sku_id)]=int(count)

        # 4. 合并前初始化记录 ( 保留redis的数据, 初始化一个选中的id列表)
        cookie_selected_ids = []

        # 5. 合并
        # {1: {count:10,selected:True}, 3:{count:30,selected:False}}

        for sku_id,count_selected_dict in cookie_cart.items():
            redis_cart[sku_id]=count_selected_dict['count']

            #  选中状态的合并

            if count_selected_dict['selected']:
                cookie_selected_ids.append(sku_id)


        # 6. 将合并的数据保存到redis中
        # 更新 hash
        # redis_cart = {sku_id:count,sku_id:count}
        redis_conn.hmset('cart_%s'%user.id,redis_cart)
        # 保存选中的id
        # cookie_selected_ids = [1,2,3,4]  因为是列表，所以需要解包
        redis_conn.sadd('cart_selected_%s' % user.id, *cookie_selected_ids)
        #  合并完后删除cookie数据
        response.delete_cookie('cart')

        return response


    return response



