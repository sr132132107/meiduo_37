from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^places/$',views.PlaceOrderAPIView.as_view()),
    url(r'^$',views.OrderAPIView.as_view()),

]