from django.shortcuts import render

# Create your views here.
from rest_framework.response import Response
from rest_framework.views import APIView

from goods.models import SKU

"""
需求:
    1.必须是登陆用户才可以访问此界面(用户地址信息确定的)
    当登陆用户访问此界面的时候,需要让前端将用户信息传递给后端

思路:
    # 1.接收用户信息,并验证用户信息
    # 2.根据用户信息获取redis中选中商品的id  [id,id,id]
    # 3.根据id获取商品的详细信息 [sku,sku,sku]
    # 4.将对象列表转换为字典
    # 5.返回相应
视图和路由

    一级   GET /orders/places/





"""
from rest_framework.permissions import IsAuthenticated
from django_redis import get_redis_connection
from .serializers import CartSKUSerializer


class PlaceOrderAPIView(APIView):

    permission_classes = [IsAuthenticated]

    def get(self,request):

        user = request.user

        # 2.根据用户信息获取redis中选中商品的id  [id,id,id]
        redis_conn = get_redis_connection('cart')

        redis_ids_count = redis_conn.hgetall('cart_%s'%user.id)

        redis_selected = redis_conn.smembers('cart_selected_%s'%user.id)

        # 类型的转换, 在类型转换过程中,我们重新组织(获取)选中的商品的信息
        redis_selected_cart = {}
        for sku_id in redis_selected:

            redis_selected_cart[int(sku_id)]=int(redis_ids_count[sku_id])

        # 根据商品id获取商品信息

        ids = redis_selected_cart.keys()

        skus = SKU.objects.filter(pk__in=ids)

        #  动态的添加商品的属性
        for sku in skus:
            sku.count = redis_selected_cart[sku.id]

        #  通过序列化器返回订单数据

        serializer = CartSKUSerializer(instance=skus,many=True)

        from decimal import Decimal
        # 因为商品价格是decimal 所以需要重新的组织数据
        freight = Decimal('10.00')
        skus=serializer.data

        data = {
            'skus':skus,
            'freight':freight
        }

        return Response(data)


#  保存订单信息
"""
需求：
    当登录用户点击提交订单的时候，前端会将收获地址、支付方式、提交给后端，后端接收数据，
    验证收据后从redis中查询商品信息，然后在保存订单信息和商品信息
步骤：
    接收数据
    验证数据
    组织数据
    保存数据库
    返回响应
视图和路由

    三级视图   POST /orders/

"""
from .serializers import OrderSerializer
class OrderAPIView(APIView):

    permission_classes = [IsAuthenticated]
    def post(self,request):

        data = request.data
        #  一级视图需要手动来传递request
        serializer = OrderSerializer(data=data,context={'request':request})
        serializer.is_valid()
        serializer.save()


        return Response(serializer.data,)



















