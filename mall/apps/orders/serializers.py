from decimal import Decimal

from django_redis import get_redis_connection
from rest_framework import serializers

from goods.models import SKU
from orders.models import OrderInfo,OrderGoods


class CartSKUSerializer(serializers.ModelSerializer):
    """
    购物车商品数据序列化器
    """
    count = serializers.IntegerField(label='数量')

    class Meta:
        model = SKU
        fields = ('id', 'name', 'default_image_url', 'price', 'count')

#  保存订单序列化器
"""
 1. 先生成订单信息
            OrderInfo
            1.1 获取用户信息 V
            1.2 获取地址信息 V
            1.3 订单id     V
            1.4 总数量,总价格,运费 V
                先把数量和总价格定义为0
            1.5 支付方式    V
            1.6 订单状态    V



"""
from django_redis import get_redis_connection


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderInfo
        fields = ('order_id', 'address', 'pay_method')
        read_only_fields = ('order_id',)
        extra_kwargs = {
            'address': {
                'write_only': True,
                'required': True,
            },
            'pay_method': {
                'write_only': True,
                'required': True
            }
        }


    #  因为在保存时触发的create方法不能满足，所以需要重写create方法
    #  组织订单数据进行保存
    def create(self, validated_data):


        #  保存订单信息
        user = self.context['request'].user

        address = validated_data['address']


        #  手动生成订单号
        from django.utils import timezone

        order_id = timezone.now().strftime('%Y%m%d%H%M%S') +'%09d'%user.id

        #     1.4 总数量,总价格,运费 V
        #         先把数量和总价格定义为0,因为后面保存商品信息时要更新总数量和总价格
        total_count = 0
        total_amount =0

        from decimal import Decimal
        freight = Decimal('10.00')

        pay_method = validated_data['pay_method']

        if pay_method == OrderInfo.PAY_METHODS_ENUM['CASH']:
            status = OrderInfo.ORDER_STATUS_ENUM['UNSEND']
        else:
            status = OrderInfo.ORDER_STATUS_ENUM['UNPAID']

        #  因为在下单的时候会涉及的多张表，需要开启事物
        from django.db import transaction
        #  设置一个保存点，用于标记事务开始的点
        with transaction.atomic():
            save_point = transaction.savepoint()  #  事务开启的点

            order = OrderInfo.objects.create(
                order_id=order_id,
                user=user,
                address=address,
                total_count=total_count,
                total_amount=total_amount,
                freight=freight,
                pay_method=pay_method,
                status=status
            )


            #  保存订单商品信息

            #  因为商品信息在redis购物车中，所以需要从redis中获取商品信息进行保存

            redis_conn = get_redis_connection('cart')

            #  hash
            redis_ids_count = redis_conn.hgetall('cart_%s'%user.id)
            # set
            redis_selected = redis_conn.smembers('cart_selected_%s'%user.id)

            #  在遍历转换数据类型的时候重新的将选中的商品id进行数据组织
            redis_cart = {}
            for sku_id in redis_selected:
                redis_cart[int(sku_id)]=int(redis_ids_count[sku_id])

            #  根据商品id来获取商品的详细信息

            sku_ids = redis_cart.keys()

            skus = SKU.objects.filter(pk__in=sku_ids)

            for sku in skus:

                count = redis_cart[sku.id]
                #  此处应该判断购买量和库存的关系
                if sku.stock < count:
                    #  出现错误，需要事务回滚到事务开始的地方
                    transaction.savepoint_rollback(save_point)

                    raise serializers.ValidationError('库存不足')
                #  购买数量满足库存后，商品的库存应该减少，销量增加
                sku.stock -= count

                sku.sales += count
                #  数据库模型类发生变化，模型应该保存
                sku.save()

                #  累加商品数量和价格(将累加计算的值 更新到订单信息中)

                order.total_count += count

                order.total_amount += (count*sku.price)

                #  保存订单商品信息
                OrderGoods.objects.create(
                    order=order,
                    sku=sku,
                    count=count,
                    price=sku.price
                )

            # 因为我们在遍历商品信息的时候 对数量和价格进行了累加计算,修改了order模型类，所以需要重新保存

            order.save()


            #  下单成功后需要提交事务

            transaction.savepoint_commit(save_point)
            #  下单成功后需要删除redis数据库中的订单的信息

            #  使用redis管道技术来删除
            pl = redis_conn.pipeline()
            pl.hdel('cart_%s' % user.id, *redis_selected)
            pl.srem('cart_selected_%s' % user.id,*redis_selected)
            pl.execute()


        return order
