from django.contrib import admin
from . import models
# Register your models here.

#  在后端管理界面注册模型类以使用后端
admin.site.register(models.ContentCategory)
admin.site.register(models.Content)