from django.conf.urls import url

from users.views import UserAddressAPIViewSet
from . import views
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework.routers import DefaultRouter
urlpatterns = [
url(r'^usernames/(?P<username>\w{5,20})/count/$',views.RegisterUsernameCountView().as_view(),name='usernamecount'),
url(r'^phones/(?P<mobile>1[3456789]\d{9})/count/$',views.RegisterPhoneCountView().as_view(),name='usermobilecount'),
url(r'^$',views.RegisterUserAPIView.as_view()),
url(r'^auths/',views.UserAuthorizationView.as_view()),
url(r'^infos/$',views.UserCenterInfoAPIView.as_view()),
url(r'^emails/$',views.UserEmailAPIView.as_view()),
url(r'^emails/verification/$',views.UserEmailVerificationAPIView.as_view()),
# url(r'^addresses/$',views.UserAddressAPIView.as_view()),
url(r'^browerhistories/$',views.UserHistoryAPIView.as_view()),

]

 # 实例化router对象
router = DefaultRouter()

#  注册router

router.register(r'addresses',UserAddressAPIViewSet,base_name=None)

#  添加路由到patterns

urlpatterns += router.urls
