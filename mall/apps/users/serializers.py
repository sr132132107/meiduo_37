from rest_framework import serializers
from users.models import User
from django_redis import get_redis_connection
import re
from rest_framework_jwt.settings import api_settings
#  定义注册序列化器

class RegisterUserSerializer(serializers.ModelSerializer):

    password2 = serializers.CharField(max_length=20,min_length=8,write_only=True,label='确认密码')
    sms_code = serializers.CharField(max_length=6,min_length=6,write_only=True,label='短信验证码')
    allow = serializers.CharField(label='是否同意',write_only=True)

    #  添加token字段用于序列化校验使用
    token = serializers.CharField(label='是否登录认证',read_only=True)


    class Meta:

        model = User

        fields = ['username','mobile','password','password2','sms_code','allow','token']

        #  设置返回的字段信息
        extra_kwargs = {
            'username': {

                'min_length': 5,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许5-20个字符的用户名',
                    'max_length': '仅允许5-20个字符的用户名',
                }
            },
            'password': {
                'write_only': True,
                'min_length': 8,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许8-20个字符的密码',
                    'max_length': '仅允许8-20个字符的密码',
                }
            }
        }


    #  单个字段验证mobile 和allow
    def validate_mobile(self,value):
        if not re.match(r'1[3-9]\d{9}',value):
            raise serializers.ValidationError('手机号格式不正确')

        return value

    def validate_allow(self,value):
        if value != 'true':
            raise serializers.ValidationError('你未同意协议')


        return value

    #  定义验证方法，对password和sms——code进行验证

    def validate(self, attrs):

        #  连接redis，获取sms——code进行验证
        redis_conn = get_redis_connection('code')
        mobile = attrs.get('mobile')
        redis_sms_code = redis_conn.get('sms_%s'%mobile)
        sms_code = attrs.get('sms_code')

        #  验证redis中获取的数据是否过期
        if redis_sms_code is None:
            raise serializers.ValidationError('短信验证码已过期')


        if redis_sms_code.decode() != sms_code:
            raise serializers.ValidationError('短信验证码验证错误')

        #  验证密码前后是否一致

        password = attrs.get('password')

        password2 = attrs.get('password2')

        if password2 != password:
            raise serializers.ValidationError('确认密码输入错误')



        return attrs


    #  因为继承Modelserializer 的create方法不能满足需求，重写create方法
    def create(self, validated_data):


        del validated_data['password2']
        del validated_data['sms_code']
        del validated_data['allow']

        user = User.objects.create(**validated_data)

        #  写入模型时，密码加密
        user.set_password(validated_data.get('password'))
        user.save()

        #  实现注册完成后自动登录功能（即用户数据入库保存之后设置token认证登录）
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        user.token = token


        return user



#  定义用户中心的序列化器

class UserCenterInfoSerializer(serializers.ModelSerializer):



    class Meta:


        model = User

        fields = ['id','username','mobile','email','email_active']




#  定义邮箱设置的序列化器


from celery_tasks.email.tasks import send_email
class UserEmailSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['email']

        extra_kwargs = {
            'email':{
                'required':True
            }
        }


    #  因需要在保存前发送email激活校验，所以需要重写update方法

    def update(self, instance, validated_data):

        email = validated_data['email']

        instance.email = email

        instance.save()

        send_email.delay(email,instance.id)


        return instance



#  定义用户地址序列化器
from .models import Address
class UserAddressSerializer(serializers.ModelSerializer):
    province = serializers.StringRelatedField(read_only=True)
    city = serializers.StringRelatedField(read_only=True)
    district = serializers.StringRelatedField(read_only=True)

    province_id = serializers.IntegerField(label='省ID', required=True)
    city_id = serializers.IntegerField(label='市ID', required=True)
    district_id = serializers.IntegerField(label='区ID', required=True)
    mobile = serializers.RegexField(label='手机号', regex=r'^1[3-9]\d{9}$')



    class Meta:

        model = Address

        exclude = ['user','create_time','update_time','is_deleted']


    #  在保存数据的时候即save（)方法触发create方法，因为user前端为传递过来
    #  系统的create方法不能满足，所以需要重写create方法

    def create(self, validated_data):

        user = self.context['request'].user

        validated_data['user_id']=user.id

        address = Address.objects.create(**validated_data)

        return address



#  定义用户浏览记录序列化器
from goods.models import SKU



class UserHistorySerializer(serializers.Serializer):

    sku_id = serializers.IntegerField(required=True,label='商品id')


    def validate(self, attrs):


        #  判断商品id是否存在
        sku_id = attrs['sku_id']

        try:
            sku = SKU.objects.get(pk=sku_id)

        except SKU.DoesNotExist:
            raise serializers.ValidationError('商品不存在')

        return attrs

    #  重写create方法  将数据保存在redis中

    def create(self, validated_data):

        #  获取用户user_id作为保存的key值
        user = self.context['request'].user
        #  获取商品sku_id作为保存的值
        sku_id = validated_data['sku_id']

        #  连接redis
        redis_conn = get_redis_connection('history')

        #  list 数据类型  list_key:value1,value2 ...

        #  因为list数据类型不能去重，所以在保存数据前需要将之前有的相同数据进行删除，以节省redis空间
        redis_conn.lrem('history_%s'%user.id,0,sku_id)

        #  将数据保存到以列表数据保存到redis中
        redis_conn.lpush('history_%s'%user.id,sku_id)

        #  因为前端限制浏览记录只有5条，即redis保存的数据只有5条
        redis_conn.ltrim('history_%s'%user.id,0,4)


        return validated_data



#  定义返回浏览商品信息的序列化器

class SKUSerializer(serializers.ModelSerializer):

    class Meta:
        model = SKU
        fields = ['id', 'name', 'price', 'default_image_url', 'comments']

















