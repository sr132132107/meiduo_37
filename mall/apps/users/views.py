from django.shortcuts import render
from django.http.request import HttpRequest
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.views import ObtainJSONWebToken

from goods.models import SKU
from .models import User, Address
# from rest_framework.generics import GenericAPIView
from .serializers import RegisterUserSerializer
# Create your views here.
'''
1,需求分析，判断用户名是否存在，返回count
2,大致过程：
    接收参数
    验证参数
    查询数据库
    返回响应
3, 请求方法和路由的确定   GET   users/?P<username>/count
4, 视图的确定
    APIView

5, 过程编码

'''
#  验证用户名是否已注册
class RegisterUsernameCountView(APIView):

    def get(self,request,username):

        count = User.objects.filter(username = username).count()

        data = {
            'count':count,
            'username':username
        }

        return Response(data)


#  验证手机号是否存在
class RegisterPhoneCountView(APIView):

    def get(self,request,mobile):

        count = User.objects.filter(mobile = mobile).count()

        data = {
            'count':count,
            'mobile':mobile
        }

        return Response(data)


#  注册
#  定义路由和视图

class RegisterUserAPIView(APIView):

    def post(self,request):

        #  使用序列化器对接收到的数据进行验证

        serializer = RegisterUserSerializer(data =request.data)

        serializer.is_valid()

        #  数据入库
        serializer.save()


        return Response(serializer.data)



#  用户中心信息的显示

"""
项目准备，增加字段，数据库模型类的迁移
需求：
    用户登录个人中心页面即登录状态，后端返回用户的个人信息：id、username、mobile、email、email_active
步骤：
    1,接收用户请求
    2,返回数据
    选用使用序列化器
视图和路由的确定：
    一级视图     GET/users/infos/

编码：
"""
from .serializers import UserCenterInfoSerializer
from rest_framework.permissions import IsAuthenticated
class UserCenterInfoAPIView(APIView):

    #  登录之后的用户才能进入到用户中心
    permission_classes = [IsAuthenticated]

    def get(self,request):

        #  查询用户获取实例化模型登录认证之后的user
        user = request.user

        serializer = UserCenterInfoSerializer(instance=user)


        return Response(serializer.data)



#  邮箱的设置
"""
需求：
    1,接收前端发来的数据email，并保存
    2,给该邮箱发送email进行校验
    3,当用户点击校验邮件链接的时候返回给后端信息，校验成功
    4,修改email_active状态，并返回前端响应

"""

from .serializers import UserEmailSerializer
class UserEmailAPIView(APIView):

    permission_classes = [IsAuthenticated]

    def put(self,request):
        # 1.接收数据
        data = request.data
        # 2.校验数据
        serializer = UserEmailSerializer(instance=request.user,data=data)
        serializer.is_valid(raise_exception=True)
        # 3.更新数据 put

        serializer.save()

        # 4.返回相应
        return Response(serializer.data)


#  定义邮箱验证成功后的视图
"""
接收数据  token
验证参数

数据入库  email_active

返回响应
"""
from rest_framework import status
from itsdangerous import TimedJSONWebSignatureSerializer as serializer
from itsdangerous import BadSignature,SignatureExpired
from mall import settings
class UserEmailVerificationAPIView(APIView):


    def get(self,request):

        token = request.query_params.get('token')

        if token is None:

            return Response(status=status.HTTP_400_BAD_REQUEST)

        s = serializer(secret_key=settings.SECRET_KEY,expires_in=3600)

        try:
            result = s.loads(token)
            # result = s.loads(token)
        except BadSignature:

            return Response(status=status.HTTP_400_BAD_REQUEST)

        #  查询用户信息
        id = result.get('id')
        email = result.get('email')

        #  数据库查询用户是否存在

        try:
            user = User.objects.get(id=id)
        except User.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        else:
            user.email_active=True
            user.save()

        return Response({'msg':'ok'})


#   用户地址的处理：增、删、改、查
#  增加用户地址
"""
需求：
    用户在登录用户中心后，增加用户地址信息，将前端传递来的用户信息传递到后端进行保存
步骤：
    1,建立地址模型，进行数据库迁移
    2,接收数据
    3,校验数据
    4,数据入库
    5,返回响应

    使用到序列化器

路由和视图的选取
    三级视图   CreateAPIView

    POST    /users/addresses/

编码：


"""
# from rest_framework.generics import CreateAPIView
# from .serializers import UserAddressSerializer
#
#
# class UserAddressAPIView(CreateAPIView):
#
#     #登录的用户，进行权限校验
#     permission_classes = [IsAuthenticated]
#
#     #  因为未用到查询，所以只需要定义序列化器属性
#
#     serializer_class = UserAddressSerializer


 # 查询用户地址信息
from .serializers import UserAddressSerializer
# from .models import Address
from rest_framework.viewsets import ModelViewSet

class UserAddressAPIViewSet(ModelViewSet):

    permission_classes = [IsAuthenticated]

    serializer_class = UserAddressSerializer

    queryset = Address.objects.all()


    def create(self, request, *args, **kwargs):
        """
        保存用户地址数据
        """
        count = request.user.addresses.count()
        if count >= 20:
            return Response({'message': '保存地址数量已经达到上限'}, status=status.HTTP_400_BAD_REQUEST)

        return super().create(request, *args, **kwargs)



    def list(self, request, *args, **kwargs):
        """
        获取用户地址列表
        """
        # 获取所有地址
        # queryset = self.get_queryset()
        # 创建序列化器
        serializer = self.get_serializer(self.queryset, many=True)
        user = self.request.user
        # 响应
        return Response({
            'user_id': user.id,
            'default_address_id': user.default_address_id,
            'limit': 20,
            'addresses': serializer.data,
        })



#  用户浏览记录

"""
需求：
    1,当登录的用户点击个人中心时，发送请求，后端返回商品的信息，按浏览的时间进行先后排序

    当前页面没有分页
    2,后端在返回商品信息前需要先将浏览的商品保存起来，即当用户点击详情页面时将用户id和商品信息，
    传送给后端进行保存
步骤：
    保存：
        1,接收数据     user_id   sku_id
        2,验证数据
        3,保存数据
            3.1,选择redis进行保存
            3.2,redis数据类型的确定   orderset有序集合和无序列表都可以，选择无序列表
        4，返回响应，使用序列化器
    展示数据：
       1,接收数据
       2,查询数据库，获取数据
        2.1, 因为保存的时候选用的是无序列表保存，获取的时候需要进行有序的获取
       3,返回响应，使用序列化器

视图和路由：

    保存时选择三级视图   CreatAPIView


    POST /users/browerhistories/


    展示时选取一级视图   APIView


    GET /users/browerhistories/


编码：


"""
from rest_framework.generics import CreateAPIView
from .serializers import UserHistorySerializer,SKUSerializer
from django_redis import get_redis_connection

#  保存用户浏览记录
class UserHistoryAPIView(CreateAPIView):

    # 登录用户
    permission_classes = [IsAuthenticated]

    serializer_class = UserHistorySerializer

    #  展示用户浏览记录
    def get(self,request):

        #  从redis中获取保存的浏览记录并返回，需要先获取rediskey值，即user

        user = request.user

        #  链接redis
        redis_conn = get_redis_connection('history')

        #  从redis中获取浏览记录，因为浏览记录是有序的，保存的list数据类型是无序的
        #  所以在从redis中获取浏览记录的时候需要遍历获取

        ids = redis_conn.lrange('history_%s'%user.id,0,4)

        skus = []
        #  根据sku_id来查询商品信息
        for id in ids:
            sku = SKU.objects.get(pk=id)
            skus.append(sku)

        serializer = SKUSerializer(instance=skus,many=True)

        return Response(serializer.data)


#  重写用户登录视图，在登录时合并cookie和redis数据
from carts.utils import merge_cookie_to_redis
class  UserAuthorizationView(ObtainJSONWebToken):

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        #  一定要是登录之后的user进行数据合并
        response = super().post(request, *args, **kwargs)

        if serializer.is_valid():

            #  此user应该为用户登录之后的user
            user = serializer.validated_data.get("user")


            response =merge_cookie_to_redis(request,user,response)


        return response














