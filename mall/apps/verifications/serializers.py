from rest_framework import serializers
from django_redis import get_redis_connection


#  定义手机短信序列化器

class RegisterSMSCodeSerializer(serializers.Serializer):

    text = serializers.CharField(label='用户输入的验证码', max_length=4, min_length=4, required=True)
    image_code_id = serializers.UUIDField(label='验证码唯一性id')




    def validate(self, attrs):
        #  连接redis数据库获取保存的图片验证码验证图片验证码是否一致

        image_code_id = attrs['image_code_id']

        redis_conn = get_redis_connection('code')

        redis_text = redis_conn.get('img_%s' % image_code_id)

        text = attrs['text']

        #  判断redis中保存的图片验证码是否已过期
        if redis_text is None:
            raise serializers.ValidationError('图片验证码以过期')
        if redis_text.decode().lower() != text:
            raise serializers.ValidationError('输入不一致')

        #  返回attrs供系统后期使用
        return attrs



