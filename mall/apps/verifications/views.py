from django.shortcuts import render
from rest_framework.views import APIView
from django.http.response import HttpResponse
from libs.captcha.captcha import Captcha
from django_redis import get_redis_connection
from . import constants
from rest_framework.generics import GenericAPIView
from .serializers import RegisterSMSCodeSerializer
from rest_framework.response import Response
# Create your views here.
#  图形验证码
#  定义类视图，接收uuid，
class RegisterImageCodeView(APIView):

    def get(self,request,image_code_id):

        # 调用第三方产生图片验证码，
        captcha = Captcha.instance()
        text, image = captcha.generate_captcha()

        # 将uuid和图片验证内容保存在redis中，连接redis

        redis_conn = get_redis_connection('code')

        # 保存数据进数据库
        redis_conn.setex('img_%s'%image_code_id,constants.SMSIMAGECODE_EXPIRE_TIME,text)

        #  返回响应
        return HttpResponse(image,content_type='image/jepg')


#  手机短信验证码
#  定义手机短信验证码类视图
from libs.yuntongxun.sms import CCP

class RegisterSMSCodeView(GenericAPIView):

    serializer_class = RegisterSMSCodeSerializer

    def get(self,request,mobile):

        query_params = request.query_params
        serializer = self.get_serializer(data= query_params)
        serializer.is_valid(raise_exception = True)

       #  生成随机的短信验证码
        import random
        sms_code = '%06d'% random.randint(0,999999)

        #  调用第三方接口发送短信

        # ccp = CCP()
        # ccp.send_template_sms(13832311214,[sms_code,5],1)
        from celery_tasks.sms.tasks import send_sms_code
        send_sms_code.delay(mobile,sms_code)
        #  将短信保存在redis中
        redis_conn = get_redis_connection('code')

        redis_conn.setex('sms_%s'%mobile,300,sms_code)

        #  返回结果

        return Response({'msg':'ok'})







