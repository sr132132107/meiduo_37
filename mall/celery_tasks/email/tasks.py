from django.core.mail import send_mail
from celery_tasks.main import app
from mall import settings


@app.task(name='send_email')
def send_email(email,id):

    subject = '美多商场激活邮件'
    message = ''
    from_email = 'qi_rui_hua@163.com'
    recipient_list = [email]

    #  链接中包含用户信息的token，因为为敏感信息和时效性，所以需要加密
    from itsdangerous import TimedJSONWebSignatureSerializer as serializer
    from mall.settings import SECRET_KEY
    s = serializer(secret_key=settings.SECRET_KEY, expires_in=3600)
    token = s.dumps({'id':id, 'email': email})

    verify_url = 'http://www.meiduo.site:8080/success_verify_email.html?token=%s'%token.decode()

    html_message = \
        '<p>尊敬的用户您好！</p>' \
        '<p>感谢您使用美多商城。</p>' \
        '<p>您的邮箱为：%s 。请点击此链接激活您的邮箱：</p>' \
        '<p><a href="%s">%s<a></p>' % (email, verify_url, verify_url)

    send_mail(subject, message, from_email, recipient_list,
              html_message=html_message)