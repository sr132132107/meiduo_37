from celery import Celery
import os

#  为celery配置django工作环境
if not os.getenv('DJANGO_SETTINGS_MODULE'):
    os.environ['DJANGO_SETTINGS_MODULE'] = 'mall.settings'



#  创建celery实例对象

app = Celery('celery_tasks')


app.config_from_object('celery_tasks.config')



#  自动加载任务,  #   自动加载发送邮件的任务

app.autodiscover_tasks(['celery_tasks.sms','celery_tasks.email','celery_tasks.html'])





